package com.buutti.filetools;

import com.buutti.mvp.CopyFileModel;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;

/**
 * @author Mika Rantakeisu
 * @version 1
 * @since 26.6.2020
 * <p>
 * This class is where characters are read from file in own thread to byteBufferLinkedList. When bytebufferLinkedList
 * reaches bytebufferSize. This thread notify all sleeping threads monitored by CopyFileModel lockObject. And after
 * that this thread stays waiting notify() call of this object from other thread.
 *
 * The lockobject is also used to track status of writing and reading in own threads
 *
 * Every exception in this thread is wrapped inside RuntimeException as cause and UncaughtExceptionHandler registered
 * to this thread.
 */

public class ReadFileThread extends Thread {
    private LinkedList<Byte> byteBufferLinkedList;
    private CopyFileModel lockObject;
    private int byteBufferSize;
    private String inputUrlText;

    public ReadFileThread(final LinkedList<Byte> byteBufferLinkedList, final CopyFileModel lockObject, int byteBufferSize, String inputUrlText) {
        this.byteBufferLinkedList = byteBufferLinkedList;
        this.lockObject = lockObject;
        this.byteBufferSize = byteBufferSize;
        this.inputUrlText = inputUrlText;
    }

    @Override
    public void run() {

        synchronized (lockObject) {
            InputStream inputStream = null;

            try {

                inputStream = new FileInputStream(inputUrlText);

                try (Reader reader = new BufferedReader(new InputStreamReader
                        (inputStream, Charset.forName(StandardCharsets.UTF_8.name())))) {
                    int c = 0;

                    while (lockObject.isReadingThreadRunning() && lockObject.getOperationSuccess()) {

                        for (int i = 0; i < byteBufferSize; i++) {
                            c = reader.read();

                            if (c != -1) {

                                byteBufferLinkedList.addFirst((byte) c);
                            } else {

                                inputStream.close();

                                reader.close();

                                lockObject.setReadingThreadState(false);

                                break;
                            }
                        }

                        //System.out.println("Reading Linkedlist buffer size: " + Long.toString(byteBufferLinkedList.size()));

                        lockObject.notify();

                        lockObject.wait();
                    }
                }

            } catch (IOException e) {

                lockObject.notifyAll();

                lockObject.setOperationSuccess(false);

                RuntimeException runtimeException = new RuntimeException(e.getMessage());

                runtimeException.initCause(e);

                throw runtimeException;
            } catch (InterruptedException e) {

                lockObject.notifyAll();

                lockObject.setOperationSuccess(false);

                RuntimeException runtimeException = new RuntimeException(e.getMessage());

                runtimeException.initCause(e);

                throw runtimeException;

            }
        }
    }
}
