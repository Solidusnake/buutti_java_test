package com.buutti.filetools;

import com.buutti.mvp.CopyFileModel;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.LinkedList;

/**
 * @author Mika Rantakeisu
 * @version 1
 * @since 26.6.2020
 * <p>
 * This class is where characters are write to file in own thread from byteBufferLinkedList. When bytebufferLinkedList
 *  is consumed to zero. This thread notify all sleeping threads monitored by CopyFileModel lockObject. And after
 *  * that this thread stays waiting notify() call of this object from other thread.
 *
 *  The lockobject is also used to track status of writing and reading in own threads
 *
 * Every exception in this thread is wrapped inside RuntimeException as cause and UncaughtExceptionHandler registered
 * to this thread.
 */

public class WriteFileThread extends Thread {

    private LinkedList<Byte> byteBufferLinkedList;
    private CopyFileModel lockObject;
    private String outputUrlText;

    public WriteFileThread(final LinkedList<Byte> byteBufferLinkedList, final CopyFileModel lockObject, final String outputUrlText) {
        this.byteBufferLinkedList = byteBufferLinkedList;
        this.lockObject = lockObject;
        this.outputUrlText = outputUrlText;
    }

    @Override
    public void run() {

        synchronized (lockObject) {

            File directory = new File(outputUrlText);

            File parent1 = directory.getParentFile();

            File directory2 = parent1.getParentFile();
            if (!directory2.exists()) {
                directory2.mkdir();
            }

            if (!parent1.exists()) {
                parent1.mkdir();
            }

            OutputStream outputStream = null;

            try {

                outputStream = new FileOutputStream(outputUrlText);

                while (lockObject.isWritingThreadRunning() && lockObject.getOperationSuccess()) {

                    final int size = byteBufferLinkedList.size();

                    for (int i = 0; i < size; i++) {
                        byte oneByte = byteBufferLinkedList.removeLast();

                        outputStream.write(oneByte);
                    }

                    //System.out.println("Writing Linkedlist buffer size: " + Long.toString(byteBufferLinkedList.size()));

                    if (lockObject.isReadingThreadRunning() == false && byteBufferLinkedList.size() == 0) {
                        lockObject.setWritingThreadState(false);

                        break;
                    }

                    lockObject.notify();

                    lockObject.wait();
                }

                outputStream.flush();

                outputStream.close();
            } catch (IOException e) {

                lockObject.notifyAll();

                lockObject.setOperationSuccess(false);

                RuntimeException runtimeException = new RuntimeException(e.getMessage());

                runtimeException.initCause(e);

                throw runtimeException;
            } catch (InterruptedException e) {

                lockObject.notifyAll();

                lockObject.setOperationSuccess(false);

                RuntimeException runtimeException = new RuntimeException(e.getMessage());

                runtimeException.initCause(e);

                throw runtimeException;
            }
        }
    }
}
