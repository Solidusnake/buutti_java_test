package com.buutti;


import com.buutti.mvp.CopyFilePresender;
import com.buutti.mvp.View;
/**
 * @author Mika Rantakeisu
 * @version 1
 * @since 26.6.2020
 * <p>
 * This class is starting point of application.
 * <p>
 * In this class instance is made of View and CopyFilePresender classes.
 * <p>
 * The app is made MVP architectural model. So View contains the UI of app. And Presender contains functionality
 * and act intermediate between Model and View.
 */

public class Mainclass {

    public static void main(String[] args) {

        final View view = new View();
        new CopyFilePresender(view);
    }

}
