package com.buutti.mvp;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import static javax.swing.GroupLayout.Alignment.*;

/**
 * @author Mika Rantakeisu
 * @version 1
 * @since 26.6.2020
 * <p>
 * The app is made MVP architectural model. So View contains the UI of app.
 * <p>
 * <p>
 * The UI consist subviews that are included to View as GroupLayout. Switch makes possible flexiple
 * placement subviews related to each others to UI.
 * <p>
 * The When CopyFile button is press. View calls all listeners, registered to ViewListener to
 * listeners arraylist
 * <p>
 * This class also contains error dialogs that show what go wrong by running the program. Also it shows Success
 * dialog that shows when filecopy operation finished succesfully. All dialogs are run inside Swing
 * Event Dispatch Thread.
 */

public class View {
    final JFrame frame = new JFrame("Buutti Test App");
    // A list of listeners subscribed to this view
    private final ArrayList<ViewListener> listeners;
    private JTextField inputUriText;
    private JTextField outputUriText;
    private JProgressBar progressBar;

    public View() {

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.setSize(800, 1000);

        frame.getContentPane().setBackground(Color.WHITE);

        GroupLayout groupLayout = new GroupLayout(frame.getContentPane());

        groupLayout.setAutoCreateGaps(true);

        groupLayout.setAutoCreateContainerGaps(true);

        final String pathToFile = "C:\\Users\\Mika Rantakeisu\\IdeaProjects\\long_text.txt";

        final String pathToFile2 = "C:\\Users\\Mika Rantakeisu\\IdeaProjects\\long_text_2.txt";

        inputUriText = new JTextField(pathToFile);

        inputUriText.setBounds(100, 100, 120, 20);

        inputUriText.setBorder(new EmptyBorder(10, 10, 10, 10));

        inputUriText.setBackground(Color.LIGHT_GRAY);

        outputUriText = new JTextField(pathToFile2);

        outputUriText.setBackground(Color.LIGHT_GRAY);

        outputUriText.setBounds(100, 100, 120, 20);

        outputUriText.setBorder(new EmptyBorder(10, 10, 10, 10));

        final JButton copyFile = new JButton("Copy file");

        copyFile.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                setInputTextFieldBackGround();

                notifyListenersOnButtonClicked(inputUriText.getText().toString(), outputUriText.getText().toString());
            }
        });

        progressBar = new JProgressBar();

        progressBar.setVisible(false);

        groupLayout.setHorizontalGroup(groupLayout.createSequentialGroup()
                .addGroup(groupLayout.createParallelGroup(LEADING).addComponent(inputUriText).addComponent(copyFile).addComponent(progressBar))
                .addGroup(groupLayout.createParallelGroup(TRAILING).addComponent(outputUriText)));

        groupLayout.setVerticalGroup(groupLayout.createSequentialGroup()
                .addGroup(groupLayout.createParallelGroup(BASELINE).addComponent(inputUriText).addComponent(outputUriText))
                .addGroup(groupLayout.createParallelGroup(BASELINE).addComponent(progressBar))
                .addGroup(groupLayout.createParallelGroup(BASELINE).addComponent(copyFile)));


        frame.getContentPane().setLayout(groupLayout);

        frame.pack();

        frame.setVisible(true);

        this.listeners = new ArrayList<ViewListener>();

        frame.setVisible(true);
    }

    // Iterate through the list, notifying each listner individualy
    private void notifyListenersOnButtonClicked(String inputUriText, String outputUriText) {
        for (final ViewListener listener : listeners) {
            listener.onButtonClicked(inputUriText, outputUriText);
        }
    }

    // Subscribe a listener
    public void addListener(final ViewListener listener) {
        listeners.add(listener);
    }

    public void showErrorMessageDialog(final String text) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {

                inputUriText.setBackground(Color.RED);

                outputUriText.setBackground(Color.RED);

                JOptionPane.showMessageDialog(frame.getContentPane(), text,
                        "ERROR", JOptionPane.ERROR_MESSAGE);
            }
        });
    }

    public void setInputTextFieldBackGround() {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {

                inputUriText.setBackground(Color.LIGHT_GRAY);

                outputUriText.setBackground(Color.LIGHT_GRAY);

            }
        });
    }

    public void showMessageDialog(String text) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                JOptionPane.showMessageDialog(frame.getContentPane(), text,
                        "ERROR", JOptionPane.ERROR_MESSAGE);
            }
        });
    }

    public void operationSuccessDialog(String time) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {

                JOptionPane.showMessageDialog(frame.getContentPane(), "Time: " + time,
                        "Success", JOptionPane.INFORMATION_MESSAGE);

            }
        });
    }

    public void setProgressToProcessBar(int progress) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {

                progressBar.setValue(progress);
            }
        });
    }

    public void setProcessBarVisibility(boolean value) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {

                progressBar.setVisible(value);
            }
        });
    }

    public interface ViewListener {
        void onButtonClicked(String inputUriText, String outputUriText);
    }
}