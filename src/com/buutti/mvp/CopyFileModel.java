package com.buutti.mvp;

/**
 * @author Mika Rantakeisu
 * @version 1
 * @since 26.6.2020
 * <p>
 * The app is made MVP architectural model. So Model contains the state of app.
 */

public class CopyFileModel {

    private String inputUrlText;

    private String outputUrlText;

    private Boolean readThread = true;

    private Boolean writeThread = true;

    private Boolean operationSuccess = true;

    public void setReadingThreadState(Boolean value) {
        readThread = value;
    }

    public void setWritingThreadState(Boolean value) {
        writeThread = value;
    }

    public boolean isReadingThreadRunning() {
        return readThread;
    }

    public boolean isWritingThreadRunning() {
        return writeThread;
    }

    public Boolean getOperationSuccess() {
        return operationSuccess;
    }

    public void setOperationSuccess(Boolean operationSuccess) {
        this.operationSuccess = operationSuccess;
    }

    public String getInputUrlText() {
        return inputUrlText;
    }

    public void setInputUrlText(String inputUrlText) {
        this.inputUrlText = inputUrlText;
    }

    public String getOutputUrlText() {
        return outputUrlText;
    }

    public void setOutputUrlText(String outputUrlText) {
        this.outputUrlText = outputUrlText;
    }
}
