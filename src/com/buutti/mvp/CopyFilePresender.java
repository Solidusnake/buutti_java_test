package com.buutti.mvp;

import com.buutti.filetools.ReadFileThread;
import com.buutti.filetools.WriteFileThread;

import java.io.IOException;
import java.util.LinkedList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * @author Mika Rantakeisu
 * @version 1
 * @since 26.6.2020
 * <p>
 * The app is made MVP architectural model. So CopyFilePresender contains the logic/functionality of app.
 * And act intermediate between View and CopyFileModel.
 */

public class CopyFilePresender implements View.ViewListener {
    private View view;

    /**
     * The class contains UncaughtExceptionHandler that catch different exception that happend inside threads. The
     * UncaughtExceptionHandler calls the View class to show Error dialog with error detail message. According the
     * cause of exception.
     */
    final Thread.UncaughtExceptionHandler uncaughtExceptionHandler = new Thread.UncaughtExceptionHandler() {
        public void uncaughtException(Thread th, Throwable ex) {

            Throwable cause = ex.getCause();

            if (cause instanceof IOException) {

                view.showErrorMessageDialog(cause.getMessage());

            } else if (cause instanceof InterruptedException) {

                view.showMessageDialog(cause.getMessage());

            } else if (cause instanceof ExecutionException) {

                view.showMessageDialog(cause.getMessage());
            } else if (cause instanceof TimeoutException) {

                view.showMessageDialog(cause.getMessage());
            }
        }
    };

    private int bufferSize = 1000;

    public CopyFilePresender(final View view) {
        this.view = view;
        view.addListener(this);
    }

    /**
     * The Presender is registered as listener to View class CopyFile button press through View.ViewListener
     * interface. This causes file copy to start. Every run new instance CopyFileModel is used as thread lock.
     * And track process of file copy.
     */
    @Override
    public void onButtonClicked(String inputUriText, String outputUriText) {

        final CopyFileModel model = new CopyFileModel();

        model.setInputUrlText(inputUriText);

        model.setOutputUrlText(outputUriText);

        runReadingAndWriting(model);
    }

    /**
     * When operation is finished. The filecopy is made. app shows Success dialog containing time to switch
     * filecopy was made. This method calculates the time on operation starttime. And shows success dialog by
     * calling View class.
     */

    public void operationMadeCallBack(long startTime) {

        final long minutes = TimeUnit.MILLISECONDS.toMinutes(startTime);

        long seconds = 0L;

        long milliSeconds = 0L;

        seconds = TimeUnit.MILLISECONDS.toSeconds(startTime);

        if (minutes > 0) {
            startTime = startTime - minutes * 60 * 100;

            seconds = TimeUnit.MILLISECONDS.toSeconds(startTime);
        }

        milliSeconds = TimeUnit.MILLISECONDS.toMillis(startTime);

        if (seconds > 0) {

            startTime = startTime - seconds * 1000;

            milliSeconds = TimeUnit.MILLISECONDS.toMillis(startTime);
        }

        String time = "";

        if (minutes == 0) {
            time = String.format("%d sec, %d milli",
                    seconds,
                    milliSeconds
            );
        } else {
            time = String.format("%d minutes, %d sec, %d milliseconds",
                    minutes,
                    seconds,
                    milliSeconds
            );
        }

        view.operationSuccessDialog(time);
    }

    /**
     * Method that causes file to copy. Input file and output file uri are encapsulated to CopyFileModel model object.
     * Given as parameters in this method call. CopyFileModel works as lock object between threads and track state of
     * file copy process. In every run new instance CopyFileModel should to be given.
     * <p>
     * The reading file happens in ReadFileThread and writing in WriteFileThread.
     * <p>
     * The byteBufferLinkedList list works as bytebuffer between threads.
     * <p>
     * The method waits for WriteFileThread to finish. And calls success dialog through View if Writeoperaion is
     * successfull.
     */

    public void runReadingAndWriting(final CopyFileModel model) {

        view.setProcessBarVisibility(true);

        view.setProgressToProcessBar(0);

        final long startTime = System.currentTimeMillis();

        final LinkedList<Byte> byteBufferLinkedList = new LinkedList<Byte>();

        final ReadFileThread readFileThread = new ReadFileThread(byteBufferLinkedList, model, bufferSize, model.getInputUrlText());

        readFileThread.setUncaughtExceptionHandler(uncaughtExceptionHandler);

        readFileThread.start();

        view.setProgressToProcessBar(5);

        final WriteFileThread writeFileThread = new WriteFileThread(byteBufferLinkedList, model, model.getOutputUrlText());

        writeFileThread.setUncaughtExceptionHandler(uncaughtExceptionHandler);

        writeFileThread.start();

        view.setProgressToProcessBar(10);
        // Lambda Runnable
        Runnable task2 = () -> {
            try {
                writeFileThread.join();
            } catch (InterruptedException e) {
                RuntimeException runtimeException = new RuntimeException(e.getMessage());

                runtimeException.initCause(e);

                throw runtimeException;
            }

            if (model.getOperationSuccess()) {

                view.setProgressToProcessBar(100);

                operationMadeCallBack(System.currentTimeMillis() - startTime);
            } else {

                view.setProgressToProcessBar(0);

                view.setProcessBarVisibility(false);
            }
        };

        Thread thread = new Thread(task2);

        thread.start();

        thread.setUncaughtExceptionHandler(uncaughtExceptionHandler);
    }
}